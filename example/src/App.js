
import React, { Component } from 'react';

import { MotorInput, Plate } from '@esrf-ui/components';

import '@esrf-ui/components/dist/index.css';
import 'bootstrap/dist/css/bootstrap.css';

export default class App extends Component {
  render () {
    return (
      <div>
        <MotorInput
          key={1}
          move={() => console.log("move")}
          value={2}
          motorName={1}
          label="Motor"
          suffix="mm"
          decimalPoints={0.1}
          state="READY"
          stop={() => console.log("move")}
          disabled={false}
          inplace
        />
        <Plate plate={ {
          name: '1',
          wellHeight: '100%',
          wellWidth: '100%',
          margin: 5,
          rowTitle: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
          colTitle: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
          type: 'square',
          title: '96 Deep Well Plate' } }
        /> 
      </div>
    )
  }
}
