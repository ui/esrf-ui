import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import external from 'rollup-plugin-peer-deps-external';
import postcss from 'rollup-plugin-postcss';
import resolve from 'rollup-plugin-node-resolve';
import url from 'rollup-plugin-url';
import svgr from '@svgr/rollup';
import includePaths from 'rollup-plugin-includepaths';
import scss from 'rollup-plugin-scss';
import copy from 'rollup-plugin-copy';

import pkg from './package.json';

let includePathOptions = {
  include: {},
  paths: ['src/'],
  external: [],
  extensions: ['.js', '.json', '.html', 'jsx', 'scss', 'css']
};

export default {
  input: 'src/index.js',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: true
    }
  ],
  plugins: [
    copy({
     targets: [
       { src: 'src/img/**/*', dest: 'dist/img' },
     ]
    }),
    scss(),    
    includePaths(includePathOptions),
    external(),
    postcss({
      modules: true
    }),
    url(),
    svgr(),
    resolve(),
    babel({
      exclude: 'node_modules/**',
      plugins: [ 'external-helpers'],
    }),
    commonjs()
  ]
}
