// Constants that are unused within this file but defined here
// for ease of reuse. However eslint complains as soon as they
// are not used within the same file. So disable eslint for this
// section

/* eslint-disable no-unused-vars */
export const MOTOR_STATE = {
  READY: 'READY',
  BUSY: 'BUSY',
  MOVING: 'MOVING',
  DISABLED: 'DISABLED',
  OFFLINE: 'OFFLINE',
  LOWLIMIT: 'LOWLIMIT',
  HIGHLIMIT: 'HIGHLIMIT',
};

export const MOTOR_STATE_DESC = {
  [MOTOR_STATE.READY]: 'Ready',
  [MOTOR_STATE.BUSY]: 'Busy',
  [MOTOR_STATE.MOVING]: 'Moving',
  [MOTOR_STATE.DISABLED]: 'Disabled',
  [MOTOR_STATE.LOWLIMIT]: 'LowLimit',
  [MOTOR_STATE.HIGHLIMIT]: 'HighLimit'
}


/* eslint-enable no-unused-vars */
