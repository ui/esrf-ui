import React from 'react';
import Badge from 'react-bootstrap/Badge';

export default class LabeledValue extends React.Component {
  render() {
    return (
      <div className="labeled-value">
        <span>
          <div>
            <Badge
              variant="secondary"
              className={`${this.props.look}-label`}
            >
              {this.props.name}
            </Badge>
          </div>
          <div>
            <Badge
              variant={this.props.level}
              className={`${this.props.look}-value`}
            >
              {this.props.value}
              {' '}
              {this.props.suffix}
            </Badge>
          </div>
        </span>
      </div>
    );
  }
}

LabeledValue.defaultProps = {
  extraInfo: {},
  value: 0,
  name: '',
  suffix: '',
  look: 'vertical',
  level: 'info'
};
