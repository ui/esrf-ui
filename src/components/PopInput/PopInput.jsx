import React from 'react';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Badge from 'react-bootstrap/Badge';

import { MOTOR_STATE } from 'constants/constants';

import DefaultInput from './PopInputDefaultInput.jsx';
import DefaultBusy from './PopInputDefaultBusy.jsx';

// import './style.css';

/**
 * A simple "Popover Input" input control, the value is displayed as text and
 * the associated input is displayed in an overlay when the text is clicked.
 *
 * Valid react properties are:
 *
 *   dataType:   The data type of the value (the input will addapt
 *               accordingly)
 *   inputSize:  Input field size, with any html unit; px, em, rem ...
 *   id:         Key used when retreiving or sending data to server
 *   name:       Name displayed in label
 *   suffix:     Suffix to display after value
 *   value:      Value
 *   state:      Current state
 *   msg:        Additional information message (coupled with state)
 *   title:      Title displayed at the top of popover
 *   placement:  Placement of Popover (left, right, bottom, top)
 *   onSave:     Callback called when user hits save button
 *   onCancel:   Callback called when user hits cancel button
 *
 * @class
 *
 */
export default class PopInput extends React.Component {
  constructor(props) {
    super(props);
    this.save = this.save.bind(this);
    this.cancel = this.cancel.bind(this);
    this.submit = this.submit.bind(this);
    this.onLinkClick = this.onLinkClick.bind(this);
    this.renderValue = this.renderValue.bind(this);
    this.renderLabel = this.renderLabel.bind(this);

    this.overlayRef = React.createRef();
    this.inputRef = React.createRef();
  }

  onLinkClick(e) {
    this.overlayRef.current.handleClick();
    e.preventDefault();
  }

  getChild(key) {
    let { children } = this.props;
    let child;

    // We need to create a real array here since react is so kind to give us
    // undefined if there is no children and an object if there is only one.
    if (this.props.children === undefined) {
      children = [];
    } else if (!Array.isArray(this.props.children)) {
      children = [this.props.children];
    }

    children.forEach((c) => {
      if (children[c].key === key) {
        child = children[c];
      }
    });

    return child;
  }

  setValue(value) {
    if (this.props.onSave !== undefined) {
      // Only update if value actually changed
      this.props.onSave(this.props.id, value);
    }
    if (this.props.state === 'IMMEDIATE' && this.overlayRefs.current) {
      this.overlayRef.current.hide();
    }
  }

  handleIdle(data) {
    // No message to display to user, hide overlay
    if (data.msg === '') {
      this.overlayRef.current.hide();
    }
  }

  handleError(data) {
    // No message to display to user, hide overlay
    if (data.msg === '') {
      this.overlayRef.current.hide();
    }
  }

  save() {
    this.setValue(this.inputRef.current.formControlRef.current.state.value);
  }

  cancel() {
    if (this.props.onCancel !== undefined && this.isBusy()) {
      this.props.onCancel(this.props.id);
    }

    if (!this.isBusy() && this.overlayRef.current) {
      this.overlayRef.current.hide();
    }
  }

  submit(event) {
    event.preventDefault();
    this.save();
  }

  inputComponent() {
    return (
      <DefaultInput
        ref={this.inputRef}
        value={this.props.value}
        precision={this.props.precision}
        step={this.props.step}
        dataType={this.props.dataType}
        inputSize={this.props.inputSize}
        inplace={this.props.inplace}
        onSubmit={this.submit}
        onCancel={this.cancel}
        onSave={this.save}
      />);
  }

  busyComponent() {
    return (<DefaultBusy onCancel={this.cancel} />);
  }

  isBusy() {
    return this.props.state === MOTOR_STATE.BUSY
      || this.props.state === MOTOR_STATE.MOVING;
  }

  isIdle() {
    return this.props.state === MOTOR_STATE.READY;
  }

  renderLabel() {
    let el = null;

    if (this.props.variant === 'horizontal') {
      el = (
        <span className={`popinput-input-label ${this.props.ref}`}>
          {this.props.name}
        </span>
      );
    } else {
      el = (
        <Badge
          variant="secondary"
          style={{ display: 'block', fontSize: '100%', marginBottom: '3px' }}
          className={`popinput-input-label ${this.props.ref}`}
        >
          {this.props.name}
        </Badge>
      );
    }

    return el;
  }

  renderValue() {
    let el = null;
    const linkClass = 'editable-click';
    let stateClass = 'input-bg-ready value-label-enter-success';

    if (this.isBusy()) {
      stateClass = 'input-bg-moving';
    }

    let value = this.props.value !== null ? parseFloat(this.props.value) : '-';

    if (value !== '-' && this.props.precision) {
      value = value.toFixed(parseInt(this.props.precision, 10));
    }

    if (this.props.variant === 'horizontal') {
      el = (
        <span
          onContextMenu={this.onLinkClick}
          key="valueLabel"
          className={`popinput-input-link ${linkClass} ${stateClass}`}
        >
          {value}
          {' '}
          {this.props.suffix}
        </span>
      );
    } else {
      el = (
        <Badge
          variant="info"
          onContextMenu={this.onLinkClick}
          key="valueLabel"
          style={{ display: 'block', fontSize: '100%', borderRadius: '0px' }}
          className={`popinput-input-link ${linkClass} ${stateClass}`}
        >
          {value}
          {' '}
          {this.props.suffix}
        </Badge>
      );
    }

    return el;
  }

  render() {
    const busyVisibility = this.isBusy() ? '' : 'hidden';
    const inputVisibility = !this.isBusy() ? '' : 'hidden';
    const title = (this.props.title === '') ? this.props.name : this.props.title;

    const popoverContent = (
      <span>
        <div className={`${inputVisibility} popinput-form-container`}>
          {this.inputComponent()}
        </div>
        <div className={inputVisibility}>{this.props.msg}</div>
        <div className={`${busyVisibility} popinput-input-loading`}>
          {this.busyComponent()}
        </div>
      </span>);

    const popover = (
      <Popover id={title} title={title}>
        { popoverContent }
      </Popover>);

    return (
      <div style={this.props.style} className={`popinput ${this.props.className} popinput-input-container`}>
        { this.props.name ? (this.renderLabel()) : null }
        <span
          className={`popinput-input-value ${this.props.id}`}
        >
          { this.props.inplace ? (
            <span>
              { popoverContent }
            </span>)
            : (
              <OverlayTrigger
                ref={this.overlayRef}
                trigger="click"
                rootClose
                placement={this.props.placement}
                overlay={popover}
              >
                {this.renderValue()}
              </OverlayTrigger>
            )
          }
        </span>
      </div>
    );
  }
}


PopInput.defaultProps = {
  className: '',
  dataType: 'number',
  inputSize: '80px',
  name: '',
  title: '',
  suffix: '',
  style: {},
  placement: 'right',
  id: undefined,
  onSave: undefined,
  onCancel: undefined,
  variant: 'horizontal',
  value: 0,
  state: 'ABORTED',
  msg: '',
  step: 0.1
};
