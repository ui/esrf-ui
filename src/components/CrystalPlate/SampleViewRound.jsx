/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
import React from 'react';

class SampleViewRound extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  wellSelected(row, col) {
    this.props.handleSelectRowSample(row, col, this.props.plate.name);
    this.props.handleSelectRowBuffer(row, col, this.props.plate.name);
  }

  wellClick() {
  }

  render() {
    return (
      <div>
        {this.props.bufferTableRows != null
          ? this.props.bufferTableRows.map((tableRow) => {
            let innerCell = null;
            if (tableRow != null && tableRow.row === this.props.platerow
              && tableRow.column === this.props.platecol
              && tableRow.plate === this.props.plate.name) {
              innerCell = (
                <circle
                  style={{
                    stroke: this.props.selectedWell === `${this.props.plate.name}:${this.props.platerow}:${this.props.platecol}` ? 'red' : 'rgb(136, 136, 136)'
                  }}
                  cx={this.props.platecol <= this.props.plate.diff
                    ? this.props.plate.wellWidth / 2 : this.props.plate.bigWidth / 2}
                  cy={this.props.platecol <= this.props.plate.diff
                    ? this.props.plate.wellHeight / 2 : this.props.plate.bigHeight / 2}
                  r={this.props.platecol <= this.props.plate.diff
                    ? this.props.plate.wellHeight / 2 : this.props.plate.bigWidth / 2}
                  strokeWidth="1"
                  fill="#0097a7"
                  onClick={() => {
                    this.wellSelected(this.props.platerow, this.props.platecol);
                    this.wellClick(`${this.props.platerow}${this.props.platecol}`);
                    this.props.handleShowBufferTable();
                  }}
                />
              );
            }
            return innerCell;
          })
          : null
        }
        {this.props.sampleTableRows != null
          ? this.props.sampleTableRows.map((tableRow) => {
            let innerCell = null;
            if (tableRow != null && tableRow.row === this.props.platerow
              && tableRow.column === this.props.platecol
              && tableRow.plate === this.props.plate.name) {
              innerCell = (
                <circle
                  style={{
                    stroke: this.props.selectedWell === `${this.props.plate.name}:${this.props.platerow}:${this.props.platecol}` ? 'red' : 'rgb(136, 136, 136)'
                  }}
                  cx={this.props.platecol <= this.props.plate.diff
                    ? this.props.plate.wellWidth / 2 : this.props.plate.bigWidth / 2}
                  cy={this.props.platecol <= this.props.plate.diff
                    ? this.props.plate.wellHeight / 2 : this.props.plate.bigHeight / 2}
                  r={this.props.platecol <= this.props.plate.diff
                    ? this.props.plate.wellHeight / 2 : this.props.plate.bigWidth / 2}
                  stroke="rgb(136, 136, 136)"
                  strokeWidth="1"
                  fill="rgb(34, 167, 78)"
                  onClick={() => {
                    this.wellSelected(this.props.platerow, this.props.platecol);
                    this.wellClick(`${this.props.platerow}${this.props.platecol}`);
                    this.props.handleShowSampleTable();
                  }}
                />
              );
            }
            return innerCell;
          })
          : null
          }
      </div>
    );
  }
}
export default SampleViewRound;
