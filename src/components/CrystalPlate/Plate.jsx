/* eslint-disable max-len */
/* eslint-disable object-curly-newline */
import React from 'react';
import SampleViewRect from './SampleViewRect.jsx';
import SampleViewRound from './SampleViewRound.jsx';

class PlateGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }


  wellClick(row, col) {
    this.props.handleSelectRowSample(row, col, this.props.plate.name);
    this.props.handleSelectRowBuffer(row, col, this.props.plate.name);
  }

  render() {
    const nbcols = this.props.plate.colTitle.length;
    const nbrows = this.props.plate.rowTitle.length;
    const plateName = this.props.plate.name;
    // plateIndex = this.props.plate.index;
    return (
      <div className="crystal-plate" style={{ paddingTop: '5px' }}>
        <div
          className="colHeader"
          style={{
            display: 'grid',
            gridTemplateColumns: `repeat(${nbcols}, 1fr)`,
          }}
        >
          {
            this.props.plate.colTitle.map(col => (
              <span
                key={`${col}`}
                style={{
                  textAlign: 'center',
                  width: this.props.plate.wellWidth,
                  height: this.props.plate.wellHeight,
                }}
              >
                {col}
              </span>
            ))
          }
        </div>
        <div style={{ display: 'flex' }}>
          {/* Plate header Row */}
          <div className="rowHeader" style={{ display: 'grid', gridTemplateRows: `repeat(${nbrows}, 1fr)` }}>
            {
              this.props.plate.rowTitle.map(row => (
                <span
                  key={`${row}`}
                  className="rowlist"
                  style={{
                    marginRight: this.props.plate.wellWidth / 2,
                    // height: this.props.plate.wellHeight,
                    // width: this.props.plate.wellWidth, height: this.props.plate.wellHeight
                    marginTop: this.props.plate.margin
                  }}
                >
                  {row}
                </span>
              ))
            }
          </div>
          <div className="grid" style={{ display: 'grid', gridTemplateColumns: `repeat(${nbcols}, 1fr)` }}>
            {
              this.props.plate.rowTitle.map((row, rowIdx) => (
                this.props.plate.colTitle.map((col, colIdx) => {
                  let cell = null;
                  if (this.props.plate.type === 'square') {
                    cell = (
                      <svg
                        key={`${row}${col}`} 
                        style={{
                          width: '100%',   
                          height: '100%',
                          marginLeft: '-1px',
                          marginBottom: this.props.plate.margin,
                        }}
                        onClick={() => {
                          this.wellClick(row, col);
                          this.props.handleLoadPlateRowsSample(this.props.plate.rowTitle);
                          this.props.handleLoadPlateColumnsSample(this.props.plate.colTitle);
                          this.props.handlePlateAddRowDefaultValueSample(plateName, rowIdx, colIdx);

                          this.props.handleLoadPlateRowsBuffer(this.props.plate.rowTitle);
                          this.props.handleLoadPlateColumnsBuffer(this.props.plate.colTitle);
                          this.props.handlePlateAddRowDefaultValueBuffer(plateName, rowIdx, colIdx);
                        }}
                      >
                        <rect
                          width={this.props.plate.wellWidth}
                          height={this.props.plate.wellHeight}
                          style={{
                            fill: 'rgba(255, 255, 255, 0)',
                            strokeWidth: '2',
                            stroke: this.props.selectedWell === `${plateName}:${row}:${col}` ? 'red' : 'rgb(136, 136, 136)'
                          }}
                        />
                        <SampleViewRect {...this.props} platerow={row} platecol={col} />
                      </svg>
                    );
                  } else if (this.props.plate.type === 'round') {
                    cell = (
                      <svg
                        style={{
                          width: this.props.plate.wellWidth,
                          height: this.props.plate.wellHeight,
                          marginBottom: this.props.plate.margin,
                          stroke: this.props.selectedWell === `${plateName}:${row}:${col}` ? 'red' : 'rgb(136, 136, 136)'
                        }}
                        onClick={() => {
                          this.props.handleLoadPlateRowsSample(this.props.plate.rowTitle);
                          this.props.handleLoadPlateColumnsSample(this.props.plate.colTitle);
                          this.props.handlePlateAddRowDefaultValueSample(plateName, rowIdx, colIdx);

                          this.props.handleLoadPlateRowsBuffer(this.props.plate.rowTitle);
                          this.props.handleLoadPlateColumnsBuffer(this.props.plate.colTitle);
                          this.props.handlePlateAddRowDefaultValueBuffer(plateName, rowIdx, colIdx);
                        }}
                      >
                        <circle
                          cx={this.props.plate.wellWidth / 2}
                          cy={this.props.plate.wellHeight / 2}
                          r={this.props.plate.wellHeight / 2}
                          // stroke="rgb(136, 136, 136)"
                          strokeWidth="1"
                          fill="rgba(253, 253, 253, 0)"
                          onClick={() => {
                            this.wellClick(row, col);
                          }}
                        />
                        <SampleViewRound {...this.props} platerow={row} platecol={col} />
                      </svg>
                    );
                  } else if (this.props.plate.type === 'roundBlock') {
                    cell = (
                      <svg
                        style={{
                          width: col <= this.props.plate.diff
                            ? this.props.plate.wellWidth : this.props.plate.bigWidth,
                          height: col <= this.props.plate.diff
                            ? this.props.plate.wellHeight : this.props.plate.bigHeight,
                          marginTop: col <= this.props.plate.diff
                            ? this.props.plate.margin : this.props.plate.margin - 10,
                          stroke: this.props.selectedWell === `${plateName}:${row}:${col}` ? 'red' : 'rgb(136, 136, 136)'

                        }}
                        onClick={() => {
                          this.props.handleLoadPlateRowsSample(this.props.plate.rowTitle);
                          this.props.handleLoadPlateColumnsSample(this.props.plate.colTitle);
                          this.props.handlePlateAddRowDefaultValueSample(plateName, rowIdx, colIdx);

                          this.props.handleLoadPlateRowsBuffer(this.props.plate.rowTitle);
                          this.props.handleLoadPlateColumnsBuffer(this.props.plate.colTitle);
                          this.props.handlePlateAddRowDefaultValueBuffer(plateName, rowIdx, colIdx);
                        }}
                      >
                        <circle
                          cx={col <= this.props.plate.diff
                            ? this.props.plate.wellWidth / 2 : this.props.plate.bigWidth / 2}
                          cy={col <= this.props.plate.diff
                            ? this.props.plate.wellHeight / 2 : this.props.plate.bigHeight / 2}
                          r={col <= this.props.plate.diff
                            ? this.props.plate.wellHeight / 2 : this.props.plate.bigWidth / 2}
                          // stroke="rgb(136, 136, 136)"
                          strokeWidth="1"
                          fill="rgba(253, 253, 253, 0)"
                          onClick={() => {
                            this.wellClick(row, col);
                          }}
                        />
                        <SampleViewRound {...this.props} platerow={row} platecol={col} />
                      </svg>
                    );
                  }
                  return cell;
                })
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}
export default PlateGrid;
