/* eslint-disable no-nested-ternary */
import React from 'react';

class SampleViewRect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  wellSelected(row, col) {
    this.props.handleSelectRowSample(row, col, this.props.plate.name);
    this.props.handleSelectRowBuffer(row, col, this.props.plate.name);
  }

  wellClick() {
  }

  render() {
    return (
      <div>
        {this.props.bufferTableRows != null
          ? this.props.bufferTableRows.map((tableRow) => {
            let innerCell = null;
            if (tableRow != null && tableRow.row === this.props.platerow
              && tableRow.column === this.props.platecol
              && tableRow.plate === this.props.plate.name) {
              innerCell = (
                <rect
                  width={this.props.plate.wellWidth}
                  height={this.props.plate.wellHeight}
                  style={{
                    fill: '#0097a7',
                    strokeWidth: '3',
                    // stroke: 'rgb(136, 136, 136)'
                    stroke: this.props.selectedWell === `${this.props.plate.name}:${this.props.platerow}:${this.props.platecol}` ? 'red' : 'rgb(136, 136, 136)'

                  }}
                  onClick={() => {
                    this.wellSelected(this.props.platerow, this.props.platecol);
                    this.wellClick(`${this.props.platecol}${this.props.platecol}`);
                    this.props.handleShowBufferTable();
                  }}
                />
              );
            }
            return innerCell;
          })
          : null
        }
        {this.props.sampleTableRows != null
          ? this.props.sampleTableRows.map((tableRow) => {
            let innerCell = null;
            if (tableRow != null && tableRow.row === this.props.platerow
              && tableRow.column === this.props.platecol
              && tableRow.plate === this.props.plate.name) {
              innerCell = (
                <rect
                  width={this.props.plate.wellWidth}
                  height={this.props.plate.wellHeight}
                  style={{
                    fill: '#66bb6a',
                    strokeWidth: '3',
                    stroke: this.props.selectedWell === `${this.props.plate.name}:${this.props.platerow}:${this.props.platecol}` ? 'red' : 'rgb(136, 136, 136)'
                  }}
                  onClick={() => {
                    this.wellSelected(this.props.platerow, this.props.platecol);
                    this.wellClick(`${this.props.platerow}${this.props.platecol}`);
                    this.props.handleShowSampleTable();
                  }}
                />
              );
            }
            return innerCell;
          })
          : null
          }

      </div>
    );
  }
}
export default SampleViewRect;
