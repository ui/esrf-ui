
import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  OverlayTrigger,
  Popover,
  Tooltip
} from 'react-bootstrap';
import {
  FaCog,
  FaAngleLeft,
  FaAngleRight,
  FaAngleUp,
  FaAngleDown,
  FaPlus,
  FaMinus,
  FaTimes
} from 'react-icons/fa';
import { MOTOR_STATE } from 'constants/constants';
import MotorInput from 'components/Motor/MotorInput.jsx';

export default class TwoAxisTranslationControl extends React.Component {
  constructor(props) {
    super(props);

    this.leftButtonOverlayRef = React.createRef();
    this.rightButtonOverlayRef = React.createRef();
    this.upButtonOverlayRef = React.createRef();
    this.downButtonOverlayRef = React.createRef();

    this.stepChange = this.stepChange.bind(this);
    this.hideOverlays = this.hideOverlays.bind(this);
  }

  stepChange(direction, step, operator) {
    const value = this.props[direction].position;
    const newValue = value + step * operator;
    this.props.move(this.props[direction].id, newValue);

    this.hideOverlays();
  }

  stopAll() {
    if (this.props.verticalMotor.sate !== MOTOR_STATE.READY) {
      this.props.stop(this.props.verticalMotor.id);
    }

    if (this.props.horizontalMotor.sate !== MOTOR_STATE.READY) {
      this.props.stop(this.props.horizontalMotor.id);
    }

    this.hideOverlays();
  }

  // Fixes bug in OverlayTrigger, events are somehow blocked when
  // a component is disabled, so mouseout is never trigger and
  // overlay not hidden
  hideOverlays() {
    this.leftButtonOverlayRef.current.hide();
    this.rightButtonOverlayRef.current.hide();
    this.upButtonOverlayRef.current.hide();
    this.downButtonOverlayRef.current.hide();
  }

  renderMotorSettings() {
    return (
      <Popover title={(<b>{this.props.popoverTitle}</b>)}>
        <div>
          <MotorInput
            move={this.props.move}
            value={this.props.verticalMotor.position}
            setStep={this.props.setStep}
            step={this.props.stepSize.vertical}
            motorName={this.props.verticalMotor.id}
            label={this.props.verticalMotor.name}
            suffix="mm"
            decimalPoints="3"
            state={this.props.verticalMotor.state}
            stop={this.props.stop}
            disabled={this.props.verticalMotor.state !== MOTOR_STATE.READY}
            inplace
          />
          <div style={{ paddingBottom: '1em' }} />
          <MotorInput
            move={this.props.move}
            value={this.props.horizontalMotor.position}
            setStep={this.props.setStep}
            step={this.props.stepSize.horizontalStep}
            motorName={this.props.horizontalMotor.id}
            label={this.props.horizontalMotor.name}
            suffix="mm"
            decimalPoints="3"
            state={this.props.horizontalMotor.state}
            stop={this.props.stop}
            disabled={this.props.horizontalMotor.state !== MOTOR_STATE.READY}
            inplace
          />
        </div>
      </Popover>
    );
  }

  renderTooltip(motor, props) {
    const status = motor.online ? motor.state : 'OFFLINE';

    return (
      <Tooltip bsPrefix="motor-tooltip tooltip" {...props}>
        {motor.name}
        <br />
        {`Status: ${status}`}
      </Tooltip>
    );
  }

  render() {
    const verticalStep = this.props.stepSize.vertical;
    const horizontalStep = this.props.stepSize.horizontal;

    return (
      <div className="arrow-control">
        <OverlayTrigger
          ref={this.upButtonOverlayRef}
          trigger="hover"
          placement="top"
          overlay={props => this.renderTooltip(this.props.verticalMotor, props)}
        >
          <Button
            variant={this.props.verticalMotor.online ? 'primary' : 'secondary'}
            onClick={() => this.stepChange('verticalMotor', verticalStep, 1)}
            disabled={
              this.props.verticalMotor.state !== MOTOR_STATE.READY
              || !this.props.verticalMotor.online
            }
            className="arrow arrow-up"
          >
            {
              this.props.variant === 'plusminus'
                ? (<FaPlus size="1.3em" />)
                : (<FaAngleUp size="1.3em" />)
            }
          </Button>
        </OverlayTrigger>
        <OverlayTrigger
          ref={this.leftButtonOverlayRef}
          trigger="hover"
          placement="bottom"
          overlay={props => this.renderTooltip(this.props.horizontalMotor, props)}
        >
          <Button
            className="arrow arrow-left"
            variant={this.props.horizontalMotor.online ? 'primary' : 'secondary'}
            disabled={
              this.props.horizontalMotor.state !== MOTOR_STATE.READY
              || !this.props.horizontalMotor.online
            }
            onClick={() => this.stepChange('horizontalMotor', horizontalStep, -1)}
          >
            {
              this.props.variant === 'plusminus'
                ? (<FaMinus size="1.3em" />)
                : (<FaAngleLeft size="1.3em" />)
            }
          </Button>
        </OverlayTrigger>
        { this.props.verticalMotor.state === MOTOR_STATE.READY
          && this.props.horizontalMotor.state === MOTOR_STATE.READY
          ? (
            <OverlayTrigger
              trigger="click"
              rootClose
              placement="bottom"
              overlay={this.renderMotorSettings()}
            >
              <Button
                className="arrow arrow-settings"
                variant={this.props.horizontalMotor.online ? 'primary' : 'secondary'}
              >
                <FaCog size="1.3em" />
              </Button>
            </OverlayTrigger>
          ) : null
        }
        { this.props.verticalMotor.state !== MOTOR_STATE.READY
          || this.props.horizontalMotor.state !== MOTOR_STATE.READY
          ? (
            <Button
              className="arrow arrow-settings"
              variant="danger"
              disabled={!this.props.horizontalMotor.online || !this.props.verticalMotor.online}
              onClick={() => this.stopAll()}
            >
              <FaTimes size="1.3em" />
            </Button>
          ) : null
        }
        <OverlayTrigger
          ref={this.rightButtonOverlayRef}
          trigger="hover"
          placement="bottom"
          overlay={props => this.renderTooltip(this.props.horizontalMotor, props)}
        >
          <Button
            className="arrow arrow-right"
            variant={this.props.horizontalMotor.online ? 'primary' : 'secondary'}
            disabled={
              this.props.horizontalMotor.state !== MOTOR_STATE.READY
              || !this.props.horizontalMotor.online
            }
            onClick={() => this.stepChange('horizontalMotor', horizontalStep, 1)}
          >
            {
              this.props.variant === 'plusminus'
                ? (<FaPlus size="1.3em" />)
                : (<FaAngleRight size="1.3em" />)
            }
          </Button>
        </OverlayTrigger>
        <OverlayTrigger
          ref={this.downButtonOverlayRef}
          trigger="hover"
          placement="bottom"
          overlay={props => this.renderTooltip(this.props.verticalMotor, props)}
        >
          <Button
            className="arrow arrow-down"
            variant={this.props.verticalMotor.online ? 'primary' : 'secondary'}
            disabled={
              this.props.verticalMotor.state !== MOTOR_STATE.READY
              || !this.props.verticalMotor.online
            }
            onClick={() => this.stepChange('verticalMotor', verticalStep, -1)}
          >
            {
              this.props.variant === 'plusminus'
                ? (<FaMinus size="1.3em" />)
                : (<FaAngleDown size="1.3em" />)
            }
          </Button>
        </OverlayTrigger>
      </div>
    );
  }
}

TwoAxisTranslationControl.propTypes = {
  move: PropTypes.func.isRequired,
  stop: PropTypes.func.isRequired,
  stepSize: PropTypes.shape({
    vertical: PropTypes.string,
    horizontal: PropTypes.string
  }),
  verticalMotor: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    position: PropTypes.string,
    state: PropTypes.number,
    online: PropTypes.bool,
  }).isRequired,
  horizontalMotor: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    position: PropTypes.string,
    state: PropTypes.number,
    online: PropTypes.bool,
  }).isRequired,
  variant: PropTypes.string,
  popoverTitle: PropTypes.string,
};

TwoAxisTranslationControl.defaultProps = {
  variant: 'arrow',
  popoverTitle: 'Motors',
  stepSize: {
    vertical: 1,
    horizontal: 1
  },
};
