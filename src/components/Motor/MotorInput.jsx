import React from 'react';
import cx from 'classnames';
import NumericInput from 'react-numeric-input';
import { Button } from 'react-bootstrap';
import { MOTOR_STATE } from 'constants/constants';
import { FaCheck, FaTimes } from 'react-icons/fa';

export default class MotorInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = { edited: false };

    this.handleKey = this.handleKey.bind(this, props.motorName);
    this.stopMotor = this.stopMotor.bind(this, props.motorName);
    this.moveMotor = this.moveMotor.bind(this, props.motorName);
    this.motorInputRef = React.createRef();
    this.motorStepInputRef = React.createRef();
  }

  /* eslint-enable react/no-set-state */
  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.motorInputRef.current.value = nextProps.value.toFixed(this.props.decimalPoints);
      this.motorInputRef.current.defaultValue = nextProps.value.toFixed(this.props.decimalPoints);
      this.setState({ edited: false });
    }
  }

  handleKey(e) {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ edited: true });

    if ([13, 38, 40].includes(e.keyCode) && this.props.state === MOTOR_STATE.READY) {
      this.setState({ edited: false });
      this.props.move(e.target.name, e.target.valueAsNumber);
      this.motorInputRef.current.value = this.props.value.toFixed(this.props.decimalPoints);
    } else if (this.props.state === MOTOR_STATE.BUSY || this.props.state === MOTOR_STATE.MOVING) {
      this.setState({ edited: false });
      this.motorInputRef.current.value = this.props.value.toFixed(this.props.decimalPoints);
    }
  }
  /* eslint-enable react/no-set-state */

  moveMotor(name) {
    this.props.move(name, this.motorInputRef.current.state.value);
  }

  stopMotor(name) {
    this.props.stop(name);
  }

  render() {
    const {
      value,
      motorName,
      step,
      suffix,
      decimalPoints
    } = this.props;
    const valueCropped = value.toFixed(decimalPoints);
    const inputCSS = cx('motor-input', {
      'input-bg-edited': this.state.edited,
      'input-bg-moving': this.props.state === MOTOR_STATE.BUSY
                         || this.props.state === MOTOR_STATE.MOVING,
      'input-bg-ready': this.props.state === MOTOR_STATE.READY,
      'input-bg-fault': this.props.state === MOTOR_STATE.FAULT
                        || this.props.state === MOTOR_STATE.OFF
                        || this.props.state === MOTOR_STATE.ALARM
                        || this.props.state === MOTOR_STATE.OFFLINE
                        || this.props.state === MOTOR_STATE.INVALID,
      'input-bg-onlimit': this.props.state === MOTOR_STATE.LOWLIMIT
                        || this.props.state === MOTOR_STATE.HIGHLIMIT
    });

    const suffixRegExp = new RegExp(`^${suffix}`);

    return (
      <div className="motor-input-container">
        <span className="motor-name">{this.props.label}</span>
        <form style={{ display: 'inline' }} onSubmit={this.handleKey} noValidate>
          <div style={{ display: 'inline-block' }}>
            <NumericInput
              ref={this.motorInputRef}
              className={inputCSS}
              name={motorName}
              precision={decimalPoints}
              step={step}
              size="20"
              value={valueCropped}
              disabled={this.props.state !== MOTOR_STATE.READY}
              style={{
                wrap: {
                  border: '1px solid #CCC',
                  display: 'inline-block',
                  height: '2.3m',
                },
                input: {
                  paddingRight: '0em',
                  marginRight: '20px',
                  height: '2.3em',
                  width: '6em',
                }
              }}
              format={num => (`${num} ${suffix}`)}
              parse={strValue => (strValue.replace(suffixRegExp, ''))}
            />
            <NumericInput
              ref={this.motorStepInputRef}
              defaultValue={0.1}
              disabled={this.props.state !== MOTOR_STATE.READY}
              style={{
                wrap: {
                  display: 'inline-block',
                  height: '2.3em',
                },
                input: {
                  height: '2.4em',
                  width: `${parseInt(decimalPoints, 10) + 3}em`,
                },
              }}
              format={num => (`${num} mm`)}
              parse={strValue => (strValue.replace(suffixRegExp, ''))}
            />
            <span className="motor-button-container">
              {this.props.state === MOTOR_STATE.READY
                ? (
                  <Button
                    className="icon-button"
                    size="xs"
                    variant="success"
                    onClick={() => this.moveMotor()}
                  >
                    <FaCheck />
                  </Button>) : null
              }
              {this.props.state !== MOTOR_STATE.READY
                ? (
                  <Button
                    className="icon-button"
                    size="xs"
                    variant="danger"
                    onClick={this.stopMotor}
                  >
                    <FaTimes />
                  </Button>)
                : null
              }
              {(this.props.setStep && this.props.state === MOTOR_STATE.READY
                && this.props.inplace)
                ? (
                  <span>
                    {this.props.step}
                    {suffix}
                  </span>)
                : null
              }
            </span>
          </div>
        </form>
      </div>
    );
  }
}
