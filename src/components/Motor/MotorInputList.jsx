import React from 'react';
import PropTypes from 'prop-types';
import { MOTOR_STATE } from 'constants/constants';
import MotorInput from 'components/Motor/MotorInput.jsx';

export default class MotorInputList extends React.Component {
  createMotorInput(motor) {
    let el = null;

    if (motor.id) {
      el = (
        <div style={{ paddingTop: '1em' }}>
          <MotorInput
            key={motor.id}
            move={this.props.move}
            value={motor.position}
            setStep={this.props.setStep}
            step={motor.stepSize}
            motorName={motor.id}
            label={motor.name}
            suffix={motor.suffix}
            decimalPoints={motor.decimalPoints}
            state={motor.state}
            stop={this.props.stop}
            disabled={motor.state !== MOTOR_STATE.READY || !motor.online}
            inplace
          />
        </div>);
    } else {
      el = null;
    }

    return el;
  }

  render() {
    return (
      <div>
        {this.props.motorList.map(motor => this.createMotorInput(motor))}
      </div>
    );
  }
}

MotorInputList.propTypes = {
  move: PropTypes.func.isRequired,
  stop: PropTypes.func.isRequired,
};
